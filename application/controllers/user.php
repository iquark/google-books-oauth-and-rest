<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function logout()
	{
		$this->session->sess_destroy();
		$data['page'] = "user/logout";
		$this->load->view('base', $data);
	}
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */