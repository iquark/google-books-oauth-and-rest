<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oauth extends CI_Controller {

	public function index()
	{
		$access_token = $this->session->userdata('access_token');
		if (!empty($access_token))
		{
			$data['access_token'] = $access_token;
			$this->load->view('common/usermenu');
			$this->load->view('oauth/connected', $data);
		}
		else
		{
			$this->load->view('oauth/pleaseconnect');
		}
	}

}

/* End of file oauth.php */
/* Location: ./application/controllers/oauth.php */