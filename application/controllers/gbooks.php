<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gbooks extends CI_Controller {

	public function index()
	{
		$this->load->helper(array('form', 'url'));
		$data['page'] = "gbooks/index";
		$this->load->view('base', $data);
	}

	public function search()
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('rest');

		$this->form_validation->set_rules('book_search', 'search', 'required');


		if ($this->form_validation->run() == FALSE)
		{
			$data['page'] = "gbooks/search";
			$this->load->view('base', $data);
		}
		else
		{
			// all gbooks api stuff
			$query = $this->input->post('book_search');
			$params['q'] = urlencode($query);
			$response = $this->rest->get('https://www.googleapis.com/books/v1/volumes', $params);

			$data['page'] = "gbooks/search";
			$data['data']['books'] = json_decode($response);
			$data['data']['query'] = $query;
			$this->load->view('base', $data);
		}
	}

	public function auth()
	{
		$this->load->library('googleOAuth');

		$this->googleoauth->auth();
	}

	public function userInfo()
	{
		$this->load->library('googleOAuth');

		$data['data']['user_info'] = $this->googleoauth->userInfo();
		$data['page'] = "oauth/user_info";
		$this->load->view('base', $data);
	}

	public function mylibrary()
	{
		$this->load->library('googleOAuth');

		$data['data']['library'] = $this->googleoauth->userBookshelves();
		$data['page'] = "gbooks/myLibrary";
		$this->load->view('base', $data);
	}
}

/* End of file gbooks.php */
/* Location: ./application/controllers/gbooks.php */