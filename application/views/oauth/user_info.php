
	<h1>User Info!</h1>

	<div id="body">
		<?php if(isSet($user_info) && !empty($user_info)): ?>
			ID: <?php echo $user_info->id; ?><br />
			email: <?php echo $user_info->email; ?><br />
			name: <?php echo $user_info->name; ?><br />
			hd: <?php echo $user_info->hd; ?><br />
		<?php else: ?>
			user not logged in <?php echo anchor('oauth/auth', 'please log in'); ?>.
		<?php endif; ?>
	</div>