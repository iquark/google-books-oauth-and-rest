<div id="usermenu">
  <?php $user_name = $this->session->userdata('user_name'); ?>
  <?php if (!empty($user_name)): ?>
    Hello <?php echo anchor('gbooks/userInfo', $user_name); ?>, <?php echo anchor('user/logout', "Logout"); ?>
  <?php else: ?>
    Please <?php echo anchor('gbooks/auth','connect'); ?>.
  <?php endif; ?>
  <ul id='menu'>
    <li><?php echo anchor('gbooks', "Search a book"); ?></li>
    <li><?php echo anchor('gbooks/myLibrary', "My library"); ?></li>
  </ul>
</div>