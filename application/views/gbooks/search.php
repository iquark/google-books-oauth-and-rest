

	<div id="body">
		<?php echo form_open('gbooks/search'); ?>

			<?php echo validation_errors(); ?>
			<input type="text" value="" name="book_search" size="50" placeholder="<?php echo (isSet($query))?$query:'Write the title of a book'; ?>" />
			<input type="submit" value="Search" name="search" />
		</form>

		<?php if(isSet($books) && isSet($books->items)): ?>
		<code>
			<?php for($idx=0; $idx<10; $idx++) :?>
				<div class='book'>
				<?php if(!empty($books->items[$idx]->volumeInfo->imageLinks->smallThumbnail)): ?>
					<img src="<?php echo $books->items[$idx]->volumeInfo->imageLinks->smallThumbnail; ?>" />
				<?php endif; ?>
					<div class="details">
						<h2><a href="<?php echo $books->items[$idx]->selfLink; ?>"><?php echo $books->items[$idx]->volumeInfo->title; ?></a></h2>
						<?php if(!empty($books->items[$idx]->volumeInfo->authors)): ?>
						<div class="author"><strong>Written by:</strong> <?php echo implode($books->items[$idx]->volumeInfo->authors, ', '); ?></div>
						<?php endif; ?>
						<?php if(!empty($books->items[$idx]->volumeInfo->publisher)): ?>
							<div class="publisher"><strong>Published by:</strong> <?php echo $books->items[$idx]->volumeInfo->publisher; ?></div>
						<?php endif; ?>
					</div>
				</div>
			<?php endfor; ?>
		</code>
		<?php endif; ?>

	</div>