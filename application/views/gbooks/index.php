
	<h1>Search in Google Books!</h1>

	<div id="body">
		<?php echo form_open('gbooks/search'); ?>

			<?php echo validation_errors(); ?>
			<input type="text" value="" placeholder="Write the title of a book" name="book_search" size="100" />
			<input type="submit" value="Search" name="search" />
		</form>

	</div>