<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rest {
  var $key;
  var $tokenAuth;

  public function setKey($key)
  {
    $this->key = $key;
  }

  public function setTokenAuth($tokenAuth)
  {
    $this->tokenAuth = $tokenAuth;
  }

	public function get($uri, $params=null, $header=null)
	{
    $service_url = $uri;

    if (!empty($params)) 
    {
      $service_url .= "?";
      foreach($params as $key=>$value)
      {
        $service_url .= "$key=$value&";
      }
    }
    
    $curl = curl_init($service_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPGET, true);
    if(!empty($header))
    {
      curl_setopt($curl, CURLOPT_HTTPHEADER, $header); 
    }
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
    $curl_response = curl_exec($curl);
    curl_close($curl);

    return $curl_response;
	}

  // POST
  public function post($uri, $post_params=null, $get_params=null, $header=array())
  {
    $service_url = $uri;

    if (!empty($get_params))
    {
      $service_url .= "?";
      foreach($get_params as $key=>$value)
      {
        $service_url .= "$key=$value&";
      }
    }

    $curl = curl_init($service_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_params);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 

    $curl_response = curl_exec($curl);
    curl_close($curl);
    
    return $curl_response;
  }

}

/* End of file rest.php */
/* Location: ./application/libraries/rest.php */