<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GoogleOAuth {
  var $uri_auth = 'https://accounts.google.com/o/oauth2/auth';
  var $uri_token = 'https://accounts.google.com/o/oauth2/token';

  public function auth()
  {
    $CI = & get_instance();
    $access_code = $CI->session->userdata('access_code');
    
    if (!empty($access_code))
    {
      $access_token = $CI->session->userdata('access_token');
      
      if (empty($access_token))
      {
        $access_token = $CI->input->get('access_token');

        if (empty($access_token))
        {
          // get the access token
          $this->exchange();
        }
      }
    }
    else
    {
      $access_code = $CI->input->get('code');

      if (!empty($access_code))
      {
        $CI->session->set_userdata('access_code', $access_code);
        // get the access token
        $this->exchange();
      }
      else
      {
        // let's start the authorization
        $this->connect();
      }
    }
  }

  private function connect()
  {
    // parameters
    $uri = $this->uri_auth;
    $params['response_type'] = "code";
    $params['client_id'] = "286572299126-9bcigcqg3s62uc5eneksvcl98l5q6mtt.apps.googleusercontent.com";
    $params['redirect_uri'] = base_url()."gbooks/auth";
    $params['scope'] = "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email";
    $params['state'] = "token";
    $params['approval_prompt'] = "force";

    if (!empty($params)) 
    {
      $uri .= "?";
      foreach($params as $key=>$value)
      {
        $uri .= "$key=".urlencode($value)."&";
      }
    }

    redirect($uri);
  }

  private function exchange()
  {
    $CI = & get_instance();
    $CI->load->library('rest');

    $access_code = $CI->session->userdata('access_code');

    // parameters
    $uri = $this->uri_token;

    $params['code'] = $access_code;
    $params['client_id'] = "286572299126-9bcigcqg3s62uc5eneksvcl98l5q6mtt.apps.googleusercontent.com";
    $params['redirect_uri'] = base_url()."gbooks/auth";
    $params['client_secret'] = "4VchfpVIo06SKsUkFrG9jlsA";
    $params['grant_type'] = "authorization_code";

    $response = $CI->rest->post($uri, $params, null);
    $response = json_decode($response);

    $access_token = (isSet($response->access_token))?$response->access_token:null;

    // creates a new session
    $CI->session->set_userdata('access_token', $access_token);
    $this->userInfo();

    redirect('oauth');
  }

  public function userInfo() 
  {
    $CI = & get_instance();

    $CI->load->library('rest');
    $access_token = $CI->session->userdata('access_token');
    $user_info = null;

    if (!empty($access_token))
    {
      $header = array("Authorization: Bearer $access_token");
      $response = $CI->rest->get('https://www.googleapis.com/oauth2/v1/userinfo', null, $header);
      $user_info = json_decode($response);

      $CI->session->set_userdata('user_name', $user_info->name);
    }
    else
    {
      $CI->session->set_userdata('user_name', "");
    }
    
    return $user_info;
  }

  public function userBookshelves()
  {
    

    $CI = & get_instance();

    $CI->load->library('rest');
    $access_token = $CI->session->userdata('access_token');
    $user_bookshelves = null;

    if (!empty($access_token))
    {
      $header = array("Authorization: Bearer $access_token");
      $response = $CI->rest->get('https://www.googleapis.com/books/v1/mylibrary/bookshelves', null, $header);
      $user_bookshelves = json_decode($response);
    }
    
    return $user_bookshelves;

  }

}

/* End of file googleOAuth.php */
/* Location: ./application/libraries/googleOAuth.php */